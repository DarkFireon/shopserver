package com.sklep.api;

import com.sklep.persistance.dto.OpinionDto;
import com.sklep.persistance.entity.Opinion;

import com.sklep.persistance.service.OpinionService;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class OpinionController {
    private final OpinionService OpinionService;

    public OpinionController(OpinionService OpinionService) {
        this.OpinionService = OpinionService;
    }

    @GetMapping("/Opinions/{device}")
    public List<OpinionDto> getOpinionsByDeviceId(@PathVariable("device") long deviceId,
                                                  @RequestParam(required = false) Integer first,
                                                  @RequestParam(required = false) Integer size) {
        List<Opinion> opinionList;
        opinionList = OpinionService.findOpinionsByDeviceId(deviceId, first, size);
        return opinionList.stream()
                .map(Opinion::convertToDto)
                .collect(Collectors.toList());
    }
    @RequestMapping(value = "/Opinion/{deviceId}", method = RequestMethod.POST)
    public OpinionDto addOpinion(@PathVariable("deviceId") long deviceId,
                                 @RequestParam String Opinion) {
        if (Opinion.equals("undefined")) return null;
        return OpinionService.addOpinion(deviceId, Opinion);
    }
    @RequestMapping(value = "/Opinion/{OpinionId}", method = RequestMethod.DELETE)
    public boolean deleteOpinion(@PathVariable("OpinionId") long OpinionId) {
        return OpinionService.deleteOpinion(OpinionId);
    }
}

package com.sklep.api;

import com.sklep.persistance.entity.Attribute;
import com.sklep.persistance.service.AttributeService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ParameterController {
    private final AttributeService attributeService;

    public ParameterController(AttributeService attributeService) {
        this.attributeService = attributeService;
    }

    @GetMapping("/parameters")
    public List<String> getAllParameters() {
        return attributeService.getAllParameters();
    }

    @PostMapping("/parameter")
    public Attribute addParameter(@RequestBody String parameter) {
        return attributeService.add(parameter);
    }

    @GetMapping("/parameters/search/{pattern}")
    public List<String> getSearchedParamsByPattern(@PathVariable("pattern") String pattern) {
        pattern = pattern.substring(0, 1).toUpperCase() + pattern.substring(1).toLowerCase();
        List<String> parameters = attributeService.getSearchedParamsByPattern(pattern);
        return parameters;
    }
}

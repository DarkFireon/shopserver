package com.sklep.api;

import com.sklep.persistance.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserController {


    private final UserService userService;

    public UserController(UserService userService) {

        this.userService = userService;
    }

    @PostMapping("authorization")
    public Boolean authorize(@RequestParam String username,@RequestParam String password) throws Exception {
        return userService.checkLogin(username,password);
    }


//    @RequestMapping(value = "/registration", method = RequestMethod.POST)
//    public ResponseEntity registration(@ModelAttribute("userForm") UserRegistrationDto userForm,
//                                       BindingResult bindingResult) {
//        userValidator.validate(userForm, bindingResult);
//        if (bindingResult.hasErrors()) {
//            ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, userValidator.getErrorCode(), getErrorMessage(bindingResult));
//            return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
//        }
//        userService.save(convertToUser(userForm));
//        return new ResponseEntity<>(userForm, HttpStatus.OK);
//    }


}

package com.sklep.api;

import com.sklep.persistance.dto.ProductDto;
import com.sklep.persistance.dto.ProductInfoDto;
import com.sklep.persistance.entity.Product;
import com.sklep.persistance.service.CategoryService;
import com.sklep.persistance.service.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService, CategoryService categoryService) {
        this.productService = productService;
    }

    @GetMapping("/devices")
    public List<ProductDto> getDevices() {
        List<Product> products = productService.getAllDevices();
        return products.stream()
                .map(Product::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/devices/popular")
    public List<ProductDto> getPopularDevices() {
        List<Product> products = productService.getAllDevices();
        return products.stream()
                .map(Product::convertToDto)
                .collect(Collectors.toList());
    }
    @GetMapping("/devices/rating/desc")
    public List<ProductDto> getBestDevices(@RequestParam(required = false) Integer page,
                                           @RequestParam(required = false) Integer size) {
        List<Product> products = productService.getBestDevicesDesc(page,size);
        return products.stream()
                .map(Product::convertToDto)
                .collect(Collectors.toList());
    }


    @GetMapping("/device/{id}")
    public ProductInfoDto getDevice(@PathVariable("id") long id) {
        Product product = productService.getDeviceById(id);
        return product.convertToInfoDto();
    }

    @GetMapping(value = "/deviceByName/{name}")
    public ProductInfoDto getDeviceByName(@PathVariable("name") String name) {
        Product product = productService.getDeviceByName(name).get();
        return product.convertToInfoDto();
    }

    @GetMapping("/devices/search/name/{pattern}")
    public List<ProductDto> getSearchedDevicesByPattern(@PathVariable("pattern") String pattern) {
        ArrayList<Product> products = productService.findDeviceNameByPattern(pattern.toLowerCase());
        return products.stream()
                .map(Product::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/devices/search/name/")
    public List<ProductDto> getSearchedDevicesByPattern() {
        return getDevices();
    }



    @PostMapping("/device")
    public ProductDto createDevice(@RequestParam String name, @RequestParam String category) {
        if (name.equals("") || name.equals("undefined") || category.equals("undefined")) return null;
        return productService.addDevice(name, category);
    }

}

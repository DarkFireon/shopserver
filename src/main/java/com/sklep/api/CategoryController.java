package com.sklep.api;

import com.sklep.persistance.entity.Category;
import com.sklep.persistance.repository.CategoryRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class CategoryController {

    private final CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @GetMapping("categories")
    public List<String> getCategories() {
        List<Category> categories = categoryRepository.findAll();
        return categories.stream()
                .map(Category::getName)
                .collect(Collectors.toList());
    }
    @GetMapping("category/{id}")
    public Optional<Category> getCategory(@PathVariable("id") long id) {
        return categoryRepository.findById(id);
    }
}

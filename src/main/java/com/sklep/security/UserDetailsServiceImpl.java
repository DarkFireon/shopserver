package com.sklep.security;

import com.sklep.persistance.entity.User;
import com.sklep.persistance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public UserPrincipal loadUserByUsername(String username) {
        User user = this.userRepository.findByUsername(username);
        return new UserPrincipal(user);
    }
}
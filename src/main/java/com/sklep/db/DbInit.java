package com.sklep.db;

import com.sklep.persistance.entity.*;
import com.sklep.persistance.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class DbInit implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final AttributeRepository attributeRepository;
    private final OpinionRepository opinionRepository;
    private final AttributeValueRepository attributeValueRepository;


    @Autowired
    public DbInit(UserRepository userRepository, CategoryRepository categoryRepository,
                  ProductRepository productRepository, AttributeRepository attributeRepository, OpinionRepository opinionRepository, AttributeValueRepository attributeValueRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
        this.attributeRepository = attributeRepository;
        this.opinionRepository = opinionRepository;
        this.attributeValueRepository = attributeValueRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {

        User artur = new User("Artur", passwordEncoder.encode("haslo"), "haslo", "ADMIN", "");
        userRepository.save(artur);

        Category category1 = new Category("lodówka");
        categoryRepository.save(category1);
        Category category2 = new Category("piekarnik");
        categoryRepository.save(category2);
        Category category3 = new Category("laptop");
        categoryRepository.save(category3);
        Category category4 = new Category("telewizor");
        categoryRepository.save(category4);


        Attribute attribute = new Attribute();
        attribute.setName("Ilość cali");
        //attribute.setCategories(Stream.of(category3,category4).collect(Collectors.toSet()));
        attributeRepository.save(attribute);

        Attribute attribute2 = new Attribute();
        attribute2.setName("HDMI");
        attribute2.setCategories(Stream.of(category3,category4).collect(Collectors.toSet()));
        attributeRepository.save(attribute2);


        Attribute attribute3 = new Attribute();
        attribute3.setName("zakres temperatury");
        //attribute3.setCategories(Stream.of(category1,category2).collect(Collectors.toSet()));
        attributeRepository.save(attribute3);


        category4.setAttributes(Stream.of(attribute).collect(Collectors.toSet()));
        categoryRepository.save(category4);



        AttributeValue attributeValue = new AttributeValue();
        attributeValue.setAttribute(attribute);
        attributeValue.setValue("14.3");
        attributeValueRepository.save(attributeValue);




        Product product = new Product();
        product.setName("BRR12M000WW");
        product.setCategory(category4);
        product.setPrice(1399.99);
        product.setDescription("niezly telewizor");


        Set attributes = product.getCategory().getAttributes();

        if(attributes.contains(attributeValue.getAttribute())){
            System.out.println("ZAWEIRAJA SIE");
            product.setProductAttributes(Stream.of(attributeValue).collect(Collectors.toSet()));
        }
        productRepository.save(product);

        attributeValue.setProducts(Stream.of(product).collect(Collectors.toSet()));
        attributeValueRepository.save(attributeValue);

        product.getProductAttributes().stream().forEach( c-> System.out.println(c.getAttribute().getName()+" "+c.getValue()));


        Opinion opinion = new Opinion();
        opinion.setUser(artur);
        opinion.setProduct(product);
        opinion.setRating(4.5);
        opinion.setComment("fajne");
        opinionRepository.save(opinion);

        addDevice(category4,artur,"Telewizor2",3.5);

        addDevice(category4,artur,"Telewizor3",4.5);

        addDevice(category4,artur,"Telewizor4",4.0);
        addDevice(category4,artur,"Telewizor5",3.9);
        addDevice(category4,artur,"Telewizor6",3.7);
    }


    public void addDevice(Category category,User user,String name, double rating){

        Product product = new Product();
        product.setName(name);
        product.setCategory(category);
        product.setPrice(1399.99);
        product.setDescription("niezly telewizor");
        product.setRating(rating);
        productRepository.save(product);

        Opinion opinion = new Opinion();
        opinion.setUser(user);
        opinion.setProduct(product);
        opinion.setRating(rating);
        opinion.setComment("fajne");
        opinionRepository.save(opinion);
    }


}

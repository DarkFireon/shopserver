package com.sklep.persistance.repository;

import com.sklep.persistance.entity.Opinion;
import com.sklep.persistance.entity.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OpinionRepository extends JpaRepository<Opinion,Long> {

    List<Opinion> findAllByProductOrderByCreatedOnDesc(Product product, Pageable pageRequest);
    Optional<Opinion> findById(long id);
    void delete(Opinion opinion);
}

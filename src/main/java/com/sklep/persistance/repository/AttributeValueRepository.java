package com.sklep.persistance.repository;

import com.sklep.persistance.entity.AttributeValue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttributeValueRepository extends JpaRepository<AttributeValue,Long> {
    AttributeValue findById(long id);
}

package com.sklep.persistance.repository;

import com.sklep.persistance.entity.Attribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.Optional;

public interface AttributeRepository extends JpaRepository<Attribute,Long> {

    @Query("select p.name " +
    " FROM Attribute p WHERE p.name like %?1%")
    ArrayList<String> findParamsNameByPattern(String pattern);

    Attribute findById(long id);
    Optional<Attribute> findByName(String name);
}

package com.sklep.persistance.repository;

import com.sklep.persistance.entity.Opinion;
import com.sklep.persistance.entity.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product,Long> {


    Optional<Product> findByName(String name);
    Optional<Product> findById(long id);
    Boolean deleteById(long id);


    @Query("select d " +
            " FROM Product d WHERE lower(d.name) like %?1%")
    ArrayList<Product> findDeviceNameByPattern(String pattern);



    List<Product> findAllByOrderByRatingDesc(Pageable pageRequest);


}

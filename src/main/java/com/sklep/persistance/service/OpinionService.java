package com.sklep.persistance.service;

import com.sklep.persistance.dto.OpinionDto;
import com.sklep.persistance.entity.Opinion;
import com.sklep.persistance.entity.Product;
import com.sklep.persistance.repository.OpinionRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OpinionService {

    private final ProductService productService;
    private final OpinionRepository OpinionRepository;

    public OpinionService(ProductService productService, OpinionRepository OpinionRepository) {
        this.productService = productService;
        this.OpinionRepository = OpinionRepository;
    }

    public List<Opinion> findOpinionsByDeviceId(long deviceId, Integer first, Integer size) {

        Product product = productService.getDeviceById(deviceId);
        return getOpinionsFromDevice(first, size, product);
    }

    private List<Opinion> getOpinionsFromDevice(Integer first, Integer size, Product product) {
        if (product == null)
            return null;
        if (first == null || size == null) {
            first = 0;
            size = 3;
        }
        return OpinionRepository.findAllByProductOrderByCreatedOnDesc(product, PageRequest.of(first, size));
    }

    public OpinionDto addOpinion(long deviceId, String OpinionValue) {
        Product product = productService.getDeviceById(deviceId);
        Opinion opinion = new Opinion();
        opinion.setComment(OpinionValue);
        opinion.setProduct(product);
        OpinionRepository.save(opinion);
        return opinion.convertToDto();
    }

    public boolean deleteOpinion(long id) {
        Optional<Opinion> Opinion = OpinionRepository.findById(id);
        if (Opinion.isPresent()) {
            OpinionRepository.delete(Opinion.get());
            return true;
        }
        return false;
    }
}

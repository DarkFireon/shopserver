package com.sklep.persistance.service;
import com.sklep.persistance.entity.Category;
import com.sklep.persistance.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    Category findByName(String category) {
        return categoryRepository.findByName(category).get();
    }
}

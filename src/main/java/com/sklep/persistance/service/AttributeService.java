package com.sklep.persistance.service;

import com.sklep.persistance.entity.Attribute;
import com.sklep.persistance.entity.AttributeValue;
import com.sklep.persistance.repository.AttributeRepository;
import com.sklep.persistance.repository.AttributeValueRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AttributeService {

    private final AttributeRepository attributeRepository;
    private final AttributeValueRepository attributeValueRepository;


    public AttributeService(AttributeRepository attributeRepository, AttributeValueRepository attributeValueRepository) {
        this.attributeRepository = attributeRepository;
        this.attributeValueRepository = attributeValueRepository;
    }

    public List<String> getAllParameters() {
        return attributeRepository.findAll().stream().map(Attribute::getName).collect(Collectors.toList());
    }



    public Attribute add(String parameterName) {
        Optional<Attribute> parameter = attributeRepository.findByName(parameterName);
        return parameter.orElseGet(() -> attributeRepository.save(new Attribute(parameterName)));
    }


    public List<String> getSearchedParamsByPattern(String pattern) {
        return attributeRepository.findParamsNameByPattern(pattern);
    }


    Attribute findById(long paramId) {
        return attributeRepository.findById(paramId);
    }

    Optional<Attribute> findByName(String parameter) {
        parameter = parameter.substring(0, 1).toUpperCase() + parameter.substring(1).toLowerCase();
        return attributeRepository.findByName(parameter);
    }

    void deleteParam(Attribute attribute) {
        attributeRepository.delete(attribute);
    }

    void deleteParamValue(AttributeValue attributeValue) {
        attributeValueRepository.delete(attributeValue);
    }

    AttributeValue findParamValueById(long id) {
        return attributeValueRepository.findById(id);
    }


}

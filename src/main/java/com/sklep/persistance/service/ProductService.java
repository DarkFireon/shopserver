package com.sklep.persistance.service;

import com.sklep.persistance.dto.ProductDto;
import com.sklep.persistance.entity.*;
import com.sklep.persistance.repository.ProductRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final CategoryService categoryService;
    private final AttributeService attributeService;

    public ProductService(ProductRepository productRepository, CategoryService categoryService, AttributeService attributeService) {
        this.productRepository = productRepository;
        this.categoryService = categoryService;
        this.attributeService = attributeService;
    }

    public List<Product> getAllDevices() {
        return productRepository.findAll();
    }

    public Product getDeviceById(long id) {
        return productRepository.findById(id).get();
    }

    public Optional<Product> getDeviceByName(String name) {
        return productRepository.findByName(name);
    }

    public Boolean deleteDeviceById(long id) {
        return productRepository.deleteById(id);
    }

    public ArrayList<Product> findDeviceNameByPattern(String pattern) {
        return productRepository.findDeviceNameByPattern(pattern);
    }

    public void save(Product product) {
        productRepository.save(product);
    }

//    @Transactional
//    public boolean deleteDeviceParam(Product product, long paramId) {
//        Attribute attribute = parameterService.findById(paramId);
//        Map<Attribute, AttributeValue> attributes = product.getParameterInfo().getAttributes();
//
//        if (attribute != null) {
//            if (attributes.containsKey(attribute)) {
//                attributes.remove(attribute);
//                product.getParameterInfo().setAttributes(attributes);
//                return true;
//            }
//        }
//        return false;
//    }



//    @Transactional
//    public Product addParamToDevice(String parameterName, String value, Product product, ValueType type) {
//        AttributeValue parameterValue = new AttributeValue(value);
//        Attribute attribute = parameterService.add(parameterName);
//        attribute.setValueType(type);
//        Map<Attribute, AttributeValue> attributes = product.getParameterInfo().getAttributes();
//        if (!attributes.containsKey(attribute)) {
//            attributes.put(attribute, parameterValue);
//        } else {
//            attributes.replace(attribute, parameterValue);
//        }
//        product.getParameterInfo().setAttributes(attributes);
//        return product;
//    }


    @Transactional
    public ProductDto addDevice(String name, String categoryName) {
        Category category = categoryService.findByName(categoryName);
        Product product = new Product();
        product.setName(name);
        product.setCategory(category);
        productRepository.save(product);
        return product.convertToDto();
    }

    public List<Product> getBestDevicesDesc(Integer page, Integer size) {
        if (page == null || size == null) {
            page = 0;
            size = 3;
        }
        return productRepository.findAllByOrderByRatingDesc(PageRequest.of(page, size));
    }
}

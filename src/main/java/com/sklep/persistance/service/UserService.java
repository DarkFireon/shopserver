package com.sklep.persistance.service;

import com.sklep.persistance.entity.User;
import com.sklep.persistance.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public boolean checkLogin(String username,String password) throws Exception{
        User user = userRepository.findByUsername(username);
        if(user == null){
            throw new Exception("Nie ma takiego konta");
        }
        if(passwordEncoder.matches(password, user.getPassword())) {
            return true;
        }
        throw new Exception("Bładne hasło");

    }


}

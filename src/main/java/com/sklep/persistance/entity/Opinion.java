package com.sklep.persistance.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sklep.SklepApplication;
import com.sklep.persistance.dto.OpinionDto;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "opinions")
public class Opinion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String comment;

    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false)
    @JsonIgnore
    private Product product;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private User user;


    @NotNull
    @Column(precision=1, scale=1)
    private double rating;

    @CreationTimestamp
    private LocalDateTime createdOn;

    public OpinionDto convertToDto() {
        return SklepApplication.modelMapper.map(this, OpinionDto.class);
    }

}

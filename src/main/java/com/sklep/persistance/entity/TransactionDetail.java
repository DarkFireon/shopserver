package com.sklep.persistance.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(name = "transactions_details")
public class TransactionDetail {
    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @NotNull
    @ManyToOne
    @JoinColumn(name = "transaction_id", referencedColumnName = "id")
    private Transaction transaction;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "device_id", referencedColumnName = "id")
    private Product product;

    int quantity;
}

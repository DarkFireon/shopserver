package com.sklep.persistance.entity;

import com.sklep.SklepApplication;
import com.sklep.persistance.dto.AttributeDto;
import com.sklep.persistance.dto.ProductDto;
import com.sklep.persistance.dto.ProductInfoDto;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Data
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    @Column(precision=10, scale=2)
    private double price;

    @NotNull
    private int stock;


    @ManyToMany(mappedBy = "products")
    private Set<AttributeValue> productAttributes = new HashSet<>();

    @NotNull
    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

    @OneToMany(mappedBy = "product")
    private List<Opinion> opinions = new ArrayList<>();

    private double rating;

   public ProductDto convertToDto(){
        ProductDto productDto = SklepApplication.modelMapper.map(this, ProductDto.class);
        productDto.setCategory(this.category.getName());
        return productDto;
    }

    public ProductInfoDto convertToInfoDto(){
        ProductInfoDto productInfoDto = SklepApplication.modelMapper.map(this, ProductInfoDto.class);
        productInfoDto.setCategory(this.category.getName());
        List<AttributeDto> attributes = new ArrayList<>();
        productAttributes.forEach(c-> attributes.add(new AttributeDto(c.getAttribute().getName(),c.getValue())));
        productInfoDto.setParameters(attributes);
        return productInfoDto;
    }
}

package com.sklep.persistance.entity;
import com.sklep.persistance.utility.ValueType;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Table(name = "attributes")
public class Attribute {

    public Attribute(){
    }
    public Attribute(String name){
        this.name=name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private ValueType valueType = ValueType.STRING;


    @NotNull
    @ManyToMany(mappedBy = "attributes")
    private Set<Category> categories = new HashSet<>();

    @OneToMany(mappedBy = "attribute")
    private List<AttributeValue> values = new ArrayList<>();


}

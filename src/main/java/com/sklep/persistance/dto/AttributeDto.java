package com.sklep.persistance.dto;

import lombok.Data;

@Data
public class AttributeDto {
    private String name;
    private String value;

    public AttributeDto(String name, String value) {
        this.name = name;
        this.value = value;

    }
}

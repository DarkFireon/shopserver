package com.sklep.persistance.dto;
import com.sklep.persistance.utility.ProductStatus;
import lombok.Data;
import java.time.LocalDateTime;

@Data
public class ProductDto {
    private long id;
    private String name;
    private String category;
    private LocalDateTime added;
    private int stock;
    private double rating;
    private String description;
    private double price;
    private ProductStatus status = ProductStatus.ZEPSUTY;
}

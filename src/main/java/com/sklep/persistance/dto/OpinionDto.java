package com.sklep.persistance.dto;
import lombok.Data;
import java.time.LocalDateTime;

@Data
public class OpinionDto {
    private long id;
    private String comment;
    private double rating;
    private LocalDateTime createdOn;
}

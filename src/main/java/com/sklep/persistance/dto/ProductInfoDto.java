package com.sklep.persistance.dto;

import com.sklep.persistance.utility.ProductStatus;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class ProductInfoDto {
    private long id;
    private String name;
    private String category;
    private int stock;
    private double rating;
    private String description;
    private double price;
    private List<AttributeDto> parameters = new ArrayList<>();
    private LocalDateTime added;
    private ProductStatus status = ProductStatus.ZEPSUTY;
}

package com.sklep;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SklepApplication {

    public static ModelMapper modelMapper = new ModelMapper();

    public static void main(String[] args) {
        SpringApplication.run(SklepApplication.class, args);
    }

}
